import java.io.*;
import java.util.*;

public class SockMerchant {

	// Complete the sockMerchant function below.
	static int sockMerchant(int n, int[] ar) {
		int[] sortedAr = ar;
		Arrays.sort(sortedAr);
		int numPairs = 0;
		int i=0, j=1;
		while(i<n && j<n) {
			if(sortedAr[i] == sortedAr[j]) {
				numPairs++;
				i++;
				j++;
			}
			i++;
			j++;
		}
		return numPairs;
	}

	public static void main(String[] args) throws IOException {
		int[] ar = {1,2,2};
		
		System.out.println(sockMerchant(3, ar));
	}
}
