
public class RepeatedString {
	// Complete the repeatedString function below.
    static long repeatedString(String s, long n) {
    	long numAs = 0;
    	if(s.length() ==0) {
    		return 0;
    	}
    	if(s.length() == 1) {
    		if(s.equals("a")) {
    			return n;
    		} else {
    			return 0;
    		}
    	}
    	
    	long timesTorepeat = n/s.length();
    	int end = s.length();
    	
    	if(timesTorepeat == 0) {
    		end = (int)n;
    	} 
    	
    	for(int i = 0; i<end; i++) {
    		if (s.charAt(i) == 'a') {
    			numAs++;
    		}
    	}
    	if(timesTorepeat != 0) {
    		numAs = numAs * timesTorepeat;
    		long remaining = (s.length() * (timesTorepeat + 1)) - n;
    		
    		for(int i = 0; i< s.length()-remaining; i++) {
    			if (s.charAt(i) == 'a') {
        			numAs++;
        		}
    		}
    	}
    	return numAs;
    }
    
    public static void main(String[] args) {
		System.out.println(repeatedString("aba", 10));
	}
}
