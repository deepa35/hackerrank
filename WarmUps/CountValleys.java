
public class CountValleys {
	static int countingValleys(int n, String s) {
		boolean isPotentialValley = false;
		if(n == 0 || n==1) {
			return 0;
		}
		int numValleys = 0;
		int[] mapArray = new int[n+1];
		mapArray[0] = 0;
		for(int i = 1; i< n; i++) {
			if(s.charAt(i-1) == 'U') {
				mapArray[i] = mapArray[i-1] + 1;
			} else {
				mapArray[i] = mapArray[i-1] - 1;
			}
		}
		
		if(mapArray[1] < 0) {
			isPotentialValley = true;
		}
		for(int i=2; i<mapArray.length; i++) {
			if(mapArray[i] == 0) {
				if(mapArray[i-1]< 0 && isPotentialValley) {
					numValleys++;
					isPotentialValley = false;
				}
			}
			if(mapArray[i] < 0 && mapArray[i-1] == 0 && !isPotentialValley) {
				isPotentialValley = true;
			}
		}
		
		return numValleys;

	}
}
