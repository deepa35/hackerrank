
public class JumpingOnClouds {
	static int jumpingOnClouds(int[] c) {
		int numOfJumps = 0;
		int currentIndex = 0;
		while (currentIndex < c.length) {
			if(currentIndex+2< c.length && c[currentIndex+2] == 0) {
				currentIndex+=2;
				numOfJumps++;
			} else if (currentIndex+1< c.length && c[currentIndex+1] == 0) {
				currentIndex++;
				numOfJumps++;
			} else {
				return numOfJumps;
			}
		}
		
		return numOfJumps;
	}
	
	public static void main(String[] args) {
		int[] c = {0, 0, 1, 0, 0, 1, 0};
		System.out.println(jumpingOnClouds(c));
	}
}
